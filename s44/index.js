//where will you fetch

let posts = [];
let count = 1;


/*
fetch() method in javascript is used to request the server and loads the webpages in the webpages.
The reques can be any API that returns the data of the format json

fetch('url', options)
	url - the url to which the request is to be made or, the routes like the url for getallusers
	options- an array properties it is an optional 


*/
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data))


//Show post function 


const showPosts = (aposts) => {
	let postEntries = ""

	aposts.forEach((post) =>{
		postEntries += `

		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>
		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//Add post

document.querySelector("#form-add-post").addEventListener("submit", (event) => {
	event.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {

		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId : 1
		}),
		header: {'Content-Type' : 'application/json'}

	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert("Post successfully addded!")
		
		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
	})

})

const editPost = (id) => {


	/*document.getElementById("demo").style.display = "block";*/

	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector("#txt-edit-id").value = id 
	document.querySelector("#txt-edit-title").value = title
	document.querySelector("#txt-edit-body").value = body
	document.querySelector("#btn-submit-update").removeAttribute('disabled');

}

//Update Post

document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
	event.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {'Content-Type' : 'application/json'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert("Post successfully updated")

		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;
		document.querySelector("#txt-edit-id").value = null;
		document.querySelector("#btn-submit-update").setAttribute('disabled', true);
	})
})

//Activity Delete Post not sure for structure to permanent delete

const deletePost = (id) => {


	/*posts = posts.filter((post) => {
		if(post.id.toString() !== id){
			return post
		}
	})*/
	
	//document.querySelector(`#post-${id}`).remove()
	
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: 'DELETE',
		body: JSON.stringify({
			userId: `${id}`
		}),
		headers: {'Content-Type' : 'application/json'}
	})
	.then((response) => response.json())
	.then((data) => {

		console.log(data)
		alert("Post successfully deleted")

	document.querySelector(`#post-${id}`).remove()
})
}

